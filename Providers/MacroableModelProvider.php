<?php

declare(strict_types=1);

namespace OctoCmsModule\Lead\Providers;

use Illuminate\Support\ServiceProvider;
use Javoscript\MacroableModels\Facades\MacroableModels;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Lead\Entities\Lead;

/**
 * Description ...
 *
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Providers
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2021
 */
class MacroableModelProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        /**
         * REGISTRY RELATIONS
         **/
        MacroableModels::addMacro(Registry::class, 'leads', function () {
            return $this->hasMany(Lead::class);
        });

        /**
         * USER RELATIONS
         **/
        MacroableModels::addMacro(User::class, 'leads', function () {
            return $this->hasMany(Lead::class);
        });
    }
}
