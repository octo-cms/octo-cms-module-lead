<?php

declare(strict_types=1);

namespace OctoCmsModule\Lead\Providers;

use Config;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use OctoCmsModule\Lead\Entities\LeadStatusTransition;
use OctoCmsModule\Lead\Interfaces\LeadServiceInterface;
use OctoCmsModule\Lead\Tests\Mocks\LeadServiceMock;

use function app;
use function array_merge;
use function config_path;
use function getBindingImplementation;
use function is_dir;
use function module_path;
use function resource_path;

/**
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Providers
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 */
class LeadServiceProvider extends ServiceProvider
{
    protected string $moduleName = 'Lead';

    protected string $moduleNameLower = 'lead';

    /**
     * Boot the application events.
     */
    public function boot(): void
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     */
    public function register(): void
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(MacroableModelProvider::class);

        if ($this->app->environment() == 'testing') {
            $this->app->bind(LeadServiceInterface::class, LeadServiceMock::class);
        } else {
            $this->app->bind(
                LeadServiceInterface::class,
                getBindingImplementation($this->moduleName, 'Services\LeadService')
            );
        }

        Relation::morphMap(
            [
                'LeadStatusTransition' => LeadStatusTransition::class
            ]
        );
    }

    /**
     * Register config.
     */
    protected function registerConfig(): void
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'),
            $this->moduleNameLower
        );
    }

    /**
     * Register views.
     */
    public function registerViews(): void
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     */
    public function registerTranslations(): void
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Register an additional directory of factories.
     */
    public function registerFactories(): void
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path($this->moduleName, 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array|mixed[]
     */
    public function provides(): array
    {
        return [];
    }

    /**
     * Name getPublishableViewPaths
     *
     * @return array|string[]
     */
    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
