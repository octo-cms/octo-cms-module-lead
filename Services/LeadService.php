<?php

namespace OctoCmsModule\Lead\Services;

use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Lead\Entities\Lead;
use OctoCmsModule\Lead\Interfaces\LeadServiceInterface;

/**
 * Class LeadService
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Services
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class LeadService implements LeadServiceInterface
{

    /**
     * Name createLeadFromProviderImport
     *
     * @param ProviderImport $providerImport ProviderImport
     *
     * @return mixed|void
     */
    public function createLeadFromProviderImport(ProviderImport $providerImport)
    {

        $providerImportData = $providerImport
            ->providerImportData()
            ->whereNotNull('registry_id')
            ->get();

        /**
         * ProviderImportData
         *
         * @var ProviderImportData $item
         */
        foreach ($providerImportData as $item) {
            $this->createLeadFromProviderImportData($item);
        }
    }

    /**
     * Name createLeadFromProviderImportData
     *
     * @param ProviderImportData $providerImportData ProviderImportData
     *
     * @return Lead
     */
    protected function createLeadFromProviderImportData(ProviderImportData $providerImportData): Lead
    {
        $lead = new Lead();
        $lead->provider()->associate($providerImportData->provider);
        $lead->registry()->associate($providerImportData->registry);
        $lead->providerImportData()->associate($providerImportData);
        $lead->date_in = $providerImportData->date;

        $lead->save();

        return $lead;
    }
}
