<?php

namespace OctoCmsModule\Lead\Interfaces;

use OctoCmsModule\Core\Entities\ProviderImport;

/**
 * Interface LeadServiceInterface
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Interfaces
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
interface LeadServiceInterface
{

    /**
     * Name createLeadFromProviderImport
     *
     * @param ProviderImport $providerImport ProviderImport
     *
     * @return mixed
     */
    public function createLeadFromProviderImport(ProviderImport $providerImport);
}
