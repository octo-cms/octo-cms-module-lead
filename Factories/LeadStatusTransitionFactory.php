<?php

namespace OctoCmsModule\Lead\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Lead\Entities\Lead;
use OctoCmsModule\Lead\Entities\LeadStatusTransition;

/**
 * Class LeadStatusTransitionFactory
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Factories
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class LeadStatusTransitionFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LeadStatusTransition::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'lead_id'         => Lead::factory(),
            'user_id'         => User::factory(),
            'status_from'     => 'from',
            'sub_status_from' => 'sub_from',
            'status_to'       => 'to',
            'sub_status_to'   => 'sub_to',
            'notes'           => null,
        ];
    }
}
