<?php

namespace OctoCmsModule\Lead\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Lead\Entities\Lead;

/**
 * Class LeadFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class LeadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Lead::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'provider_id'             => Provider::factory(),
            'registry_id'             => Registry::factory(),
            'user_id'                 => User::factory(),
            'provider_import_data_id' => ProviderImportData::factory(),
            'date_in'                 => Carbon::yesterday()->format('Y-m-d'),
            'date_out'                => Carbon::now()->format('Y-m-d'),
            'status'                  => 'free',
            'sub_status'              => null,
            'notes'                   => null,
        ];
    }
}
