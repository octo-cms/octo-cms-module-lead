<?php

namespace OctoCmsModule\Lead\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Lead\Entities\Lead;
use OctoCmsModule\Lead\Entities\LeadEventLog;
use OctoCmsModule\Lead\Entities\LeadStatusTransition;

/**
 * Class LeadStatusTransitionFactory
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Factories
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class LeadEventLogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LeadEventLog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'lead_id' => Lead::factory(),
            'user_id' => User::factory()
        ];
    }
}
