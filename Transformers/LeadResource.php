<?php

namespace OctoCmsModule\Lead\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OctoCmsModule\Core\Transformers\ProviderImportDataResource;
use OctoCmsModule\Core\Transformers\ProviderResource;
use OctoCmsModule\Core\Transformers\RegistryResource;
use OctoCmsModule\Core\Transformers\UserResource;

/**
 * Class LeadResource
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Transformers
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class LeadResource extends JsonResource
{
    /**
     * Name toArray
     *
     * @param Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                      => $this->id,
            'registry_id'             => $this->registry_id,
            'provider_id'             => $this->provider_id,
            'user_id'                 => $this->user_id,
            'provider_import_data_id' => $this->provider_import_data_id,
            'status'                  => $this->status,
            'sub_status'              => $this->sub_status,
            'date_in'                 => optional($this->date_in)->format('Y-m-d'),
            'date_out'                => optional($this->date_out)->format('Y-m-d'),
            'note'                    => $this->note,
            'registry'                => new RegistryResource($this->whenLoaded('registry')),
            'provider'                => new ProviderResource($this->whenLoaded('provider')),
            'user'                    => new UserResource($this->whenLoaded('user')),
            'provider_import_data'    => new ProviderImportDataResource(
                $this->whenLoaded('providerImportData')
            ),
            'lead_status_transition'  => LeadStatusTransitionResource::collection(
                $this->whenLoaded('leadStatusTransition')
            ),
        ];
    }
}
