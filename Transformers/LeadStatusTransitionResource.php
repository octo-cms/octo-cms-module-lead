<?php

namespace OctoCmsModule\Lead\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OctoCmsModule\Core\Transformers\UserResource;

/**
 * Class LeadStatusTransitionResource
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Transformers
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class LeadStatusTransitionResource extends JsonResource
{
    /**
     * Name toArray
     *
     * @param Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                      => $this->id,
            'user_id'                 => $this->user_id,
            'lead_id'                 => $this->lead_id,
            'provider_import_data_id' => $this->provider_import_data_id,
            'status_from'             => $this->status_from,
            'sub_status_from'         => $this->sub_status_from,
            'status_to'               => $this->status_to,
            'sub_status_to'           => $this->sub_status_to,
            'note'                    => $this->note,
            'lead'                    => new LeadResource($this->whenLoaded('provider')),
            'user'                    => new UserResource($this->whenLoaded('user')),
        ];
    }
}
