<?php

namespace OctoCmsModule\Lead\Tests\Services\LeadService;

use Exception;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Notifications\UserRegistrationNotification;
use Illuminate\Support\Facades\Notification;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Services\UserService;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Lead\Entities\Lead;
use OctoCmsModule\Lead\Services\LeadService;

/**
 * Class RegisterUserTest
 *
 * @package OctoCmsModule\Core\Tests\Services\UserService
 */
class CreateLeadFromProviderImportTest extends TestCase
{

    /**
     * Name testCreateLeadFromProviderImport
     *
     * @return void
     */
    public function testCreateLeadFromProviderImport()
    {

        (new LeadService())
            ->createLeadFromProviderImport(ProviderImport::factory()
                ->has(ProviderImportData::factory()->count(3))
                ->create());

        $this->assertCount(3, Lead::all());
    }
}
