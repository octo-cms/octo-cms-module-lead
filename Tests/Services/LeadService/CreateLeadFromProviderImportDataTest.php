<?php

namespace OctoCmsModule\Lead\Tests\Services\LeadService;

use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Lead\Services\LeadService;

/**
 * Class RegisterUserTest
 *
 * @package OctoCmsModule\Core\Tests\Services\UserService
 */
class CreateLeadFromProviderImportDataTest extends TestCase
{

    /**
     * Name testCreateLeadFromProviderImportData
     *
     * @return void
     * @throws \ReflectionException
     */
    public function testCreateLeadFromProviderImportData()
    {

        /** @var LeadService $leadService */
        $leadService = new LeadService();
        /** @var ProviderImportData $providerImportData */
        $providerImportData = ProviderImportData::factory()->create();
        $this->invokeMethod(
            $leadService,
            'createLeadFromProviderImportData',
            ['providerImportData' => $providerImportData]
        );

        $this->assertDatabaseHas('lead_leads', [
            'id'                      => 1,
            'provider_id'             => $providerImportData->provider->id,
            'registry_id'             => $providerImportData->registry->id,
            'provider_import_data_id' => $providerImportData->id,
            'user_id'                 => null,
            'date_in'                 => $providerImportData->date->format('Y-m-d H:i:s'),
            'date_out'                => null,
            'status'                  => null,
            'sub_status'              => null,
            'notes'                   => null
        ]);
    }
}
