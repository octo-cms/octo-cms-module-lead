<?php

namespace OctoCmsModule\Lead\Tests\Entities;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Lead\Entities\Lead;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class RegistryTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class UserTest extends TestCase
{
    /**
     * Name testRegistryHasManyLeads
     *
     * @return void
     */
    public function testUserHasManyLeads()
    {
        /** @var User $user */
        $user = User::factory()
            ->has(Lead::factory()->count(3), 'leads')
            ->create();

        $user->load('leads');

        $this->assertInstanceOf(Collection::class, $user->leads);
        $this->assertInstanceOf(Lead::class, $user->leads->first());
    }
}
