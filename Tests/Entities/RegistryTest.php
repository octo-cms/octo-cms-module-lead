<?php

namespace OctoCmsModule\Lead\Tests\Entities;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Lead\Entities\Lead;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class RegistryTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class RegistryTest extends TestCase
{
    /**
     * Name testRegistryHasManyLeads
     *
     * @return void
     */
    public function testRegistryHasManyLeads()
    {
        /** @var Registry $registry */
        $registry = Registry::factory()
            ->has(Lead::factory()->count(3), 'leads')
            ->create();

        $registry->load('leads');

        $this->assertInstanceOf(Collection::class, $registry->leads);
        $this->assertInstanceOf(Lead::class, $registry->leads->first());
    }
}
