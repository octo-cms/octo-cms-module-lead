<?php

namespace OctoCmsModule\Lead\Tests\Entities;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Lead\Entities\Lead;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Lead\Entities\LeadEventLog;
use OctoCmsModule\Lead\Entities\LeadStatusTransition;

/**
 * Class LeadTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class LeadStatusTransitionTest extends TestCase
{
    /**
     * Name testLeadStatusTransitionBelongsTo
     *
     * @return void
     */
    public function testLeadStatusTransitionBelongsTo()
    {
        /** @var LeadStatusTransition $leadStatus */
        $leadStatus=LeadStatusTransition::factory()->create();
        $this->assertInstanceOf(User::class, $leadStatus->user);
        $this->assertInstanceOf(Lead::class, $leadStatus->lead);
    }

    public function testLeadStatusTransitionMorph()
    {
        /** @var LeadStatusTransition $leadStatusTransition */
        $leadStatusTransition = LeadStatusTransition::factory()->create();

        $leadStatusTransition->leadEventLog()->save(LeadEventLog::factory()->create());

        $leadStatusTransition->load('leadEventLog');

        $this->assertInstanceOf(LeadEventLog::class, $leadStatusTransition->leadEventLog);
    }

    public function testLeadEventLogCreation()
    {
        /** @var LeadStatusTransition $leadStatusTransition */
        $leadStatusTransition = LeadStatusTransition::factory()->create();

        $this->assertDatabaseHas('lead_lead_event_logs', [
            'lead_id'       => $leadStatusTransition->lead->id,
            'user_id'       => $leadStatusTransition->user->id,
            'loggable_type' => 'LeadStatusTransition',
            'loggable_id'   => $leadStatusTransition->id,
        ]);
    }
}
