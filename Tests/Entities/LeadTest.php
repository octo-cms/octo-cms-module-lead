<?php

namespace OctoCmsModule\Lead\Tests\Entities;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Lead\Entities\Lead;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Lead\Entities\LeadEventLog;
use OctoCmsModule\Lead\Entities\LeadStatusTransition;

/**
 * Class LeadTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class LeadTest extends TestCase
{

    /**
     * Name testLeadBelongsTo
     *
     * @return void
     */
    public function testLeadBelongsTo()
    {
        /** @var Lead $lead */
        $lead = Lead::factory()
            ->has(LeadStatusTransition::factory()->count(3))
            ->has(LeadEventLog::factory()->count(2))
            ->create();

        $this->assertInstanceOf(Registry::class, $lead->registry);
        $this->assertInstanceOf(Provider::class, $lead->provider);
        $this->assertInstanceOf(User::class, $lead->user);
        $this->assertInstanceOf(ProviderImportData::class, $lead->providerImportData);

        $this->assertInstanceOf(Collection::class, $lead->leadStatusTransitions);
        $this->assertInstanceOf(LeadStatusTransition::class, $lead->leadStatusTransitions->first());

        $this->assertInstanceOf(Collection::class, $lead->leadEventLogs);
        $this->assertInstanceOf(LeadEventLog::class, $lead->leadEventLogs->first());
    }

    public function testLeadStatusTransitions()
    {
        /** @var Lead $lead */
        $lead = Lead::factory()
            ->state([
                'status'     => 'status',
                'sub_status' => 'sub_status',
            ])
            ->create();

        $lead->wasRecentlyCreated = false;

        $lead->update([
            'status'     => 'new_status',
            'sub_status' => 'new_sub_status',
        ]);

        $this->assertDatabaseHas('lead_lead_status_transitions', [
            'lead_id' => $lead->id,
            'status_from' => 'status',
            'sub_status_from'=> 'sub_status',
            'status_to'=> 'new_status',
            'sub_status_to' => 'new_sub_status',
        ]);
    }
}
