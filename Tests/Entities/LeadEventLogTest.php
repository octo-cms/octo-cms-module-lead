<?php

namespace OctoCmsModule\Lead\Tests\Entities;

use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Lead\Entities\Lead;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Lead\Entities\LeadEventLog;
use OctoCmsModule\Lead\Entities\LeadStatusTransition;

/**
 * Class LeadTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class LeadEventLogTest extends TestCase
{
    /**
     * Name testLeadBelongsTo
     *
     * @return void
     */
    public function testLeadBelongsTo()
    {
        /** @var LeadEventLog $leadEventLog */
        $leadEventLog = LeadEventLog::factory()->create();

        $this->assertInstanceOf(User::class, $leadEventLog->user);
        $this->assertInstanceOf(Lead::class, $leadEventLog->lead);
    }

    public function testMorphTo()
    {

        /** @var LeadEventLog $leadEventLog */
        $leadEventLog = LeadEventLog::factory()->create();

        $leadEventLog->leadEventLoggable()->associate(LeadStatusTransition::factory()->create());

        $leadEventLog->load('leadEventLoggable');

        $this->assertInstanceOf(LeadStatusTransition::class, $leadEventLog->leadEventLoggable);
    }
}
