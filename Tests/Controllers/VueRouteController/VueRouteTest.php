<?php

namespace OctoCmsModule\Lead\Tests\Controllers\VueRouteController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class VueRouteTest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Lead\Tests\Controllers\VueRouteController
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class VueRouteTest extends TestCase
{

    /**
     * Name dataProvider
     *
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $providers[] = ['admin.vue-route.lead.leads'];

        return $providers;
    }

    /**
     * Name testRoutes
     * @param string $route
     *
     * @dataProvider dataProvider
     *
     * @return void
     */
    public function testRoutes(string $route)
    {
        Sanctum::actingAs(self::createAdminUser());

        $this->withoutMix();

        $response = $this->json('GET', $route);

        $response->assertStatus(Response::HTTP_OK);
    }
}
