<?php

namespace OctoCmsModule\Lead\Tests\Controllers\LeadController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Lead\Entities\Lead;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Lead\Tests\Controllers\LeadController
 */
class DatatableTest extends TestCase
{

    public function testDatatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        Lead::factory()->count(15)->create();

        $this->datatableTest('admin.datatables.leads');
    }
}
