<?php

namespace OctoCmsModule\Lead\Tests\Mocks;

use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Lead\Entities\Lead;
use OctoCmsModule\Lead\Interfaces\LeadServiceInterface;

/**
 * Class TagService
 *
 * @package OctoCmsModule\Core\Services
 */
class LeadServiceMock implements LeadServiceInterface
{


    /**
     * Name createLeadFromProviderImport
     * @param ProviderImport $providerImport ProviderImport
     *
     * @return mixed|void
     */
    public function createLeadFromProviderImport(ProviderImport $providerImport)
    {
        // TODO: Implement createLeadFromProviderImport() method.
    }
}
