<?php

namespace OctoCmsModule\Lead\Http\Controllers;

use Illuminate\Contracts\View\View;
use App\Http\Controllers\Controller;

/**
 * Class VueRouteController
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Http\Controllers\
 * @author   Adriano Camacaro <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class VueRouteController extends Controller
{
    /**
     * @return View
     */
    public function leads()
    {
        return view(
            'admin::vue-full-page',
            ['script' => 'lead/leads']
        );
    }
}
