<?php

namespace OctoCmsModule\Lead\Http\Controllers\V1;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use OctoCmsModule\Lead\Entities\Lead;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Lead\Transformers\LeadResource;

/**
 * Class LeadController
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Http\Controllers\V1
 * @author   Adriano Camacaro <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class LeadController extends Controller
{
    /**
     * Name datatableIndex
     *
     * @param DatatableRequest          $request          DatatableRequest
     * @param DatatableServiceInterface $datatableService DatatableServiceInterface
     *
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function datatableIndex(
        DatatableRequest $request,
        DatatableServiceInterface $datatableService
    )
    {
        $this->authorize('datatableIndex', Lead::class);

        $leads = Lead::query();

        $datatableDTO = $datatableService->getDatatableDTO($request->validated(), $leads);

        $collection = $datatableDTO->builder->get();

        $collection
            ->load('provider')
            ->load('user')
            ->load('registry', 'registry.privateRegistry');

        $datatableDTO->collection = LeadResource::collection($collection);

        return response()->json($datatableDTO, Response::HTTP_OK);
    }
}
