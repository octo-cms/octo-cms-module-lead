<?php

return [
    'name' => 'Lead',
    'admin' => [
        'sidebar' => [
            [
                'order'  => 6,
                'label'  => 'leads',
                'childs' => [
                    [
                        'label' => 'list',
                        'route' => 'admin.vue-route.lead.leads',
                    ],
                ],
            ],
        ],
    ],
];
