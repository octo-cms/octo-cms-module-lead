import Registry from "../../../../Core/Resources/vue/models/registry";

const Lead = {
    id: null,
    provider_id: null,
    registry_id: null,
    status: '',
    date_in: null,
    date_out: null,
    provider: {},
    registry: Registry,
};

export default Lead
