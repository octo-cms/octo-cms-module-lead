const routes = {
    DATATABLES: {
        leads: 'api/admin/v1/datatables/lead/leads',
    },
};

export {
    routes
}
