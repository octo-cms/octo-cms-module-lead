<?php

namespace OctoCmsModule\Lead\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Interfaces\ProviderServiceInterface;
use OctoCmsModule\Lead\Interfaces\LeadServiceInterface;

/**
 * Class CreateLeadFromProviderImportJob
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Jobs
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CreateLeadFromProviderImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * ProviderImport
     *
     * @var ProviderImport
     */
    protected $providerImport;


    /**
     * SyncProviderImportWithRegistryJob constructor.
     *
     * @param ProviderImport $providerImport ProviderImport
     */
    public function __construct(ProviderImport $providerImport)
    {
        $this->providerImport = $providerImport;
    }

    /**
     * Name handle
     *
     * @param LeadServiceInterface $leadService LeadServiceInterface
     *
     * @return void
     */
    public function handle(LeadServiceInterface $leadService)
    {
        $leadService->createLeadFromProviderImport($this->providerImport);
    }
}
