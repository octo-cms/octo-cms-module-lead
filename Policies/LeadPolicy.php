<?php

namespace OctoCmsModule\Lead\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Interfaces\PermissionServiceInterface;

/**
 * Class LeadPolicy
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Policies
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class LeadPolicy
{
    use HandlesAuthorization;

    protected $permissionService;

    /**
     * RolePolicy constructor.
     * @param PermissionServiceInterface $permissionService PermissionServiceInterface
     */
    public function __construct(PermissionServiceInterface $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * Name datatableIndex
     * @param User $user User Logged
     *
     * @return bool
     */
    public function datatableIndex(User $user)
    {
        return true;
    }
}
