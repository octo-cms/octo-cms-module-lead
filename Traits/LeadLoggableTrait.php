<?php

namespace OctoCmsModule\Lead\Traits;

use OctoCmsModule\Lead\Entities\LeadEventLog;

/**
 * Trait LeadLoggableTrait
 *
 * @category Octo
 * @package  OctoCmsModule\Lead\Traits
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
trait LeadLoggableTrait
{
    /**
     * Name leadEventLog
     *
     * @return mixed
     */
    public function leadEventLog()
    {
        return $this->morphOne(
            LeadEventLog::class,
            'leadEventLoggable',
            'loggable_type',
            'loggable_id'
        );
    }
}
