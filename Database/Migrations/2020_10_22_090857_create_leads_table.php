<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('provider_id')->unsigned();
            $table->bigInteger('registry_id')->unsigned();
            $table->bigInteger('provider_import_data_id')->unsigned()->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->date('date_in');
            $table->date('date_out')->nullable();
            $table->string('status', 50)->nullable();
            $table->string('sub_status', 50)->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();

            $table->foreign('provider_id')->references('id')
                ->on('providers')->onDelete('cascade');
            $table->foreign('registry_id')->references('id')
                ->on('registry')->onDelete('cascade');
            $table->foreign('provider_import_data_id')->references('id')
                ->on('provider_import_data')->onDelete('cascade');
            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('set null');
        });

        Schema::create('lead_lead_status_transitions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('lead_id')->unsigned();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('status_from', 50);
            $table->string('sub_status_from', 50)->nullable();
            $table->string('status_to', 50);
            $table->string('sub_status_to', 50)->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('set null');

            $table->foreign('lead_id')->references('id')
                ->on('lead_leads')->onDelete('cascade');

        });

        Schema::create('lead_lead_event_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('lead_id')->unsigned();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('loggable_type')->nullable();
            $table->bigInteger('loggable_id')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade');

            $table->foreign('lead_id')->references('id')
                ->on('lead_leads')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_lead_event_logs');
        Schema::dropIfExists('lead_lead_status_transactions');
        Schema::dropIfExists('lead_leads');
    }
}
