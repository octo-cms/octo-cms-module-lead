<?php

namespace OctoCmsModule\Lead\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Lead\Factories\LeadFactory;

/**
 * OctoCmsModule\Lead\Entities\Lead
 *
 * @property int $id
 * @property int $provider_id
 * @property int $registry_id
 * @property \Illuminate\Support\Carbon $date_in
 * @property \Illuminate\Support\Carbon $date_out
 * @property string $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Lead\Entities\LeadStatusTransition[] $leadStatusTransitions
 * @property-read int|null $lead_status_transitions_count
 * @property-read Provider $provider
 * @property-read ProviderImportData $providerImportData
 * @property-read Registry $registry
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Lead newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Lead newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Lead query()
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereRegistryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereStatus($value)
 * @mixin \Eloquent
 * @property int|null $provider_import_data_id
 * @property int|null $user_id
 * @property string|null $sub_status
 * @property string|null $notes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereProviderImportDataId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereSubStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereUserId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Lead\Entities\LeadEventLog[] $leadEventLogs
 * @property-read int|null $lead_event_logs_count
 * @property string $date
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereDateIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereDateOut($value)
 * @method static \OctoCmsModule\Lead\Factories\LeadFactory factory(...$parameters)
 */
class Lead extends Model
{
    use HasFactory;

    protected $table = 'lead_leads';

    protected $fillable = [
        'status',
        'sub_status',
        'date_in',
        'date_out',
        'notes'
    ];

    protected $casts = [
        'date_in'  => 'date:Y-m-d',
        'date_out' => 'date:Y-m-d'
    ];

    /**
     * Name booted
     *
     * @return void
     */
    protected static function booted()
    {
        static::saved(function (Lead $lead) {

            if (!$lead->wasRecentlyCreated
                && ($lead->isDirty('status') || $lead->isDirty('sub_status'))) {
                /** @var User $currentUser */
                $currentUser = Auth::user();
                //phpcs:disable
                $leadStatusTransition = new LeadStatusTransition();
                $leadStatusTransition->lead()->associate($lead);
                if (!empty($currentUser)) {
                    $leadStatusTransition->user()->associate($currentUser);
                }
                $leadStatusTransition->status_from = $lead->getOriginal('status');
                $leadStatusTransition->sub_status_from = $lead->getOriginal('sub_status');
                $leadStatusTransition->status_to = $lead->status;
                $leadStatusTransition->sub_status_to = $lead->sub_status;
                $leadStatusTransition->save();
                //phpcs:enable
            }

        });
    }

    /**
     * Name newFactory
     *
     * @return LeadFactory
     */
    protected static function newFactory(): LeadFactory
    {
        return LeadFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function registry(): BelongsTo
    {
        return $this->belongsTo(Registry::class);
    }

    /**
     * @return BelongsTo
     */
    public function provider(): BelongsTo
    {
        return $this->belongsTo(Provider::class);
    }

    /**
     * @return BelongsTo
     */
    public function providerImportData(): BelongsTo
    {
        return $this->belongsTo(ProviderImportData::class);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Name leadStatusTransitions
     *
     * @return HasMany
     */
    public function leadStatusTransitions(): HasMany
    {
        return $this->hasMany(LeadStatusTransition::class);
    }

    /**
     * Name eventLogs
     *
     * @return HasMany
     */
    public function leadEventLogs(): HasMany
    {
        return $this->hasMany(LeadEventLog::class);
    }
}
