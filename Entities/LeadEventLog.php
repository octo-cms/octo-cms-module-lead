<?php

namespace OctoCmsModule\Lead\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Lead\Factories\LeadEventLogFactory;

/**
 * Class LeadEventLog
 * Description .
 * 
 * ..
 *
 * @category Octo
 * @package OctoCmsModule\Lead\Entities
 * @author Pasi Daniele <pasidaniele@gmail.com>
 * @license copyright Octopus Srl 2021
 * @link https://octopus.srl
 * @property-read \OctoCmsModule\Lead\Entities\Lead $lead
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|LeadEventLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LeadEventLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LeadEventLog query()
 * @mixin \Eloquent
 * @property-read Model|\Eloquent $leadEventLoggable
 * @property int $id
 * @property int $lead_id
 * @property int $user_id
 * @property string|null $loggable_type
 * @property int|null $loggable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LeadEventLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadEventLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadEventLog whereLeadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadEventLog whereLoggableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadEventLog whereLoggableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadEventLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadEventLog whereUserId($value)
 * @method static \OctoCmsModule\Lead\Factories\LeadEventLogFactory factory(...$parameters)
 */
class LeadEventLog extends Model
{
    use HasFactory;

    protected $table = 'lead_lead_event_logs';

    protected $fillable = [

    ];


    /**
     * Name newFactory
     *
     * @return LeadEventLogFactory
     */
    protected static function newFactory(): LeadEventLogFactory
    {
        return LeadEventLogFactory::new();
    }

    /**
     * Name user
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Name lead
     *
     * @return BelongsTo
     */
    public function lead(): BelongsTo
    {
        return $this->belongsTo(Lead::class);
    }

    /**
     * Name leadEventLoggable
     *
     * @return MorphTo
     */
    public function leadEventLoggable(): MorphTo
    {
        return $this->morphTo(__FUNCTION__, 'loggable_type', 'loggable_id');
    }
}
