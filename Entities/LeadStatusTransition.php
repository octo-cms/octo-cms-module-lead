<?php

namespace OctoCmsModule\Lead\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Lead\Factories\LeadStatusTransitionFactory;
use OctoCmsModule\Lead\Traits\LeadLoggableTrait;


/**
 * OctoCmsModule\Lead\Entities\LeadStatusTransition
 *
 * @property-read \OctoCmsModule\Lead\Entities\Lead              $lead
 * @property-read User                                           $user
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatusTransition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatusTransition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatusTransition query()
 * @mixin \Eloquent
 * @property int                                                 $id
 * @property int                                                 $lead_id
 * @property int|null                                            $user_id
 * @property string                                              $status_from
 * @property string                                              $sub_status_from
 * @property string                                              $status_to
 * @property string                                              $sub_status_to
 * @property string|null                                         $notes
 * @property \Illuminate\Support\Carbon|null                     $created_at
 * @property \Illuminate\Support\Carbon|null                     $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatusTransition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatusTransition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatusTransition whereLeadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatusTransition whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatusTransition whereStatusFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatusTransition whereStatusTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatusTransition whereSubStatusFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatusTransition whereSubStatusTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatusTransition whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LeadStatusTransition whereUserId($value)
 * @property-read \OctoCmsModule\Lead\Entities\LeadEventLog|null $leadEventLog
 * @method static \OctoCmsModule\Lead\Factories\LeadStatusTransitionFactory factory(...$parameters)
 */
class LeadStatusTransition extends Model
{

    use HasFactory, LeadLoggableTrait;

    protected $table = 'lead_lead_status_transitions';

    protected $fillable = [
        'notes',
        'status_from',
        'sub_status_from',
        'status_to',
        'sub_status_to',
    ];

    /**
     * Name newFactory
     *
     * @return LeadStatusTransitionFactory
     */
    protected static function newFactory()
    {
        return LeadStatusTransitionFactory::new();
    }

    /**
     * Name booted
     *
     * @return void
     */
    protected static function booted()
    {
        static::saved(function (LeadStatusTransition $leadStatusTransition) {
            $leadEventLog = new LeadEventLog();
            $leadEventLog->user()->associate($leadStatusTransition->user);
            $leadEventLog->lead()->associate($leadStatusTransition->lead);
            $leadEventLog->leadEventLoggable()->associate($leadStatusTransition);
            $leadEventLog->save();
        });
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function lead()
    {
        return $this->belongsTo(Lead::class);
    }
}
