<?php

Route::group(['prefix' => env('MIX_ADMIN_PREFIX'), 'middleware' => ['auth-admin']], function () {
    Route::group(['prefix' => 'lead'], function () {
        Route::get(
            'leads',
            getRouteAction("Lead", "VueRouteController", 'leads')
        )
            ->name('admin.vue-route.lead.leads');
    });
});
