<?php

Route::group(['prefix' => 'admin/v1'], function () {

    Route::group(['middleware' => ['auth:sanctum']], function () {

        Route::group(['prefix' => 'datatables/lead'], function () {
            Route::post(
                'leads',
                getRouteAction("Lead", "V1\LeadController", 'datatableIndex')
            )->name('admin.datatables.leads');
        });
    });
});
