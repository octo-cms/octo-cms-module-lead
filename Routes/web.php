<?php

use OctoCmsModule\Lead\Http\Controllers\VueRouteController;

Route::group(['prefix' => env('MIX_ADMIN_PREFIX'), 'middleware' => ['auth-admin']], function () {
    Route::group(['prefix' => 'lead'], function () {
        Route::get(
            'leads', [VueRouteController::class, 'leads'])->name('admin.vue-route.lead.leads');
    });
});
