<?php

use OctoCmsModule\Lead\Http\Controllers\V1\LeadController;

Route::group(['prefix' => 'admin/v1'], function () {

    Route::group(['middleware' => ['auth:sanctum']], function () {

        Route::group(['prefix' => 'datatables/lead'], function () {
            Route::post('leads', [LeadController::class, 'datatableIndex'])
                ->name('admin.datatables.leads');
        });
    });
});
